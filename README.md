![](avysis.png) [beta]

Avysis is a small antivirus built with PowerShell. It uses the [MalwareBazaar API](https://bazaar.abuse.ch/api/), which is really useful (doesn't use a key, no rate-limit).

## How to install

Currently, the installation process is to go to the releases tab and download the EXE. Then run it.

### Compile yourself

#### Quick compile

You need ps2exe and Inno Setup (in path).
```powershell
git clone https://github.com/avysis/avysis.git; cd avysis; .\build
```

#### Normal compile

You need ps2exe and Inno Setup (in path). Then run build.cmd.

#### Install

Run the setup EXE.
